package org.riksa.android.rxdaggerviewbinding.retrofit;

import org.riksa.android.rxdaggerviewbinding.model.Repo;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by riksa on 28/10/15.
 */
public interface GitHubService {
    // from http://square.github.io/retrofit/
    @GET("/users/{user}/repos")
    Observable<List<Repo>> listRepos(@Path("user") String user);
}
