package org.riksa.android.rxdaggerviewbinding.model;

import java.util.Date;

/**
 * Created by riksa on 28/10/15
 * Model for Retrofit to use in debug variant.
 */

public class Repo {
    //    "name": "Aardvark",
//    "full_name": "square/Aardvark",
    public String name;

    public String full_name;

    public Date created_at;
}
