package org.riksa.android.rxdaggerviewbinding.dagger;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.riksa.android.rxdaggerviewbinding.BuildConfig;
import org.riksa.android.rxdaggerviewbinding.model.LoadingProgress;
import org.riksa.android.rxdaggerviewbinding.model.Repo;
import org.riksa.android.rxdaggerviewbinding.model.ViewModelCard;
import org.riksa.android.rxdaggerviewbinding.retrofit.GitHubService;

import java.util.List;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by riksa on 28/10/15.
 * VariantModule for "debug" buildtype
 */
@Module
public class VariantModule {

    public static final String ENDPOINT = "https://api.github.com";
    public static final String REPOS = "square";

    @Provides
    Observable<List<ViewModelCard>> provideViewModelListObservable(GitHubService gitHubService) {
        // get repos from githubservice and map them to cards
        final DateTimeFormatter dateTimeFormatter = DateTimeFormat.shortDateTime(); // dateformat could be something we want to inject...

        final Observable<List<Repo>> squareRepos = gitHubService.listRepos(REPOS);
        final Observable<Repo> repoObservable = squareRepos.flatMap(new Func1<List<Repo>, Observable<Repo>>() {
            @Override
            public Observable<Repo> call(List<Repo> repos) {
                return Observable.from(repos);
            }
        });

        final Observable<ViewModelCard> map = repoObservable.map(new Func1<Repo, ViewModelCard>() {
            @Override
            public ViewModelCard call(Repo repo) {
                return new ViewModelCard(
                        repo.full_name,
                        dateTimeFormatter.print(new DateTime(repo.created_at)));
            }
        });

        return map.toList();
    }

    @Provides
    public GitHubService provideGitHubService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(BuildConfig.ENABLE_LOGGING ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        return restAdapter.create(GitHubService.class);
    }

    @Provides
    Observable<LoadingProgress> provideLoadingProgressObservable(GitHubService gitHubService) {
        final Observable<List<Repo>> squareRepos = gitHubService.listRepos(REPOS);

        return squareRepos
                .flatMap(flatmapRepoListToLoadingProgress());

    }

    @NonNull
    private Func1<Long, Observable<LoadingProgress>> flatmapIndexToLoadingProgress(final List<Repo> repos, final long total) {
        return new Func1<Long, Observable<LoadingProgress>>() {
            @Override
            public Observable<LoadingProgress> call(Long index) {
                return Observable.just(
                        new LoadingProgress((int) ((100 * index) / total), repos.get(index.intValue()).full_name));
            }
        };
    }

    @NonNull
    private Func1<List<Repo>, Observable<LoadingProgress>> flatmapRepoListToLoadingProgress() {
        return new Func1<List<Repo>, Observable<LoadingProgress>>() {
            @Override
            public Observable<LoadingProgress> call(final List<Repo> repos) {
                final int total = repos.size();
                return Observable.interval(500, TimeUnit.MILLISECONDS)
                        .take(total)
                        .flatMap(flatmapIndexToLoadingProgress(repos, total));
            }
        };
    }
}
