package org.riksa.android.rxdaggerviewbinding.dagger;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.riksa.android.rxdaggerviewbinding.model.LoadingProgress;
import org.riksa.android.rxdaggerviewbinding.model.ViewModelCard;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by riksa on 28/10/15.
 * VariantModule for "uitest" buildtype
 */
@Module
public class VariantModule {
    @Provides
    Observable<List<ViewModelCard>> provideViewModelListObservable() {
        final List<ViewModelCard> cardList = new LinkedList<>();
        final DateTime now = DateTime.now();
        final DateTimeFormatter dateTimeFormatter = DateTimeFormat.shortDateTime();
        for (int c = 0; c < 100; c++) {
            cardList.add(
                    new ViewModelCard(
                            "Title " + c,
                            dateTimeFormatter.print(now.minusHours(c))));
        }

        return Observable
                .just(cardList)
                .delay(500, TimeUnit.MILLISECONDS);

    }

    @Provides
    Observable<LoadingProgress> provideLoadingProgressObservable() {
        final List<LoadingProgress> loadingProgresses = Arrays.asList(
                new LoadingProgress(0, "Connecting to server..."),
                new LoadingProgress(7, "... Fetching foo ..."),
                new LoadingProgress(26, "... Cooking foo ..."),
                new LoadingProgress(46, "... Baking bar ..."),
                new LoadingProgress(56, "... Preparing stuff ..."),
                new LoadingProgress(86, "... Formatting C:\\ ..."),
                new LoadingProgress(100, "... Done"));

        return Observable.create(new Observable.OnSubscribe<LoadingProgress>() {
            @Override
            public void call(final Subscriber<? super LoadingProgress> subscriber) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            for (LoadingProgress loadingProgress : loadingProgresses) {
                                subscriber.onNext(loadingProgress);
                                Thread.sleep(1500); // blocking thread, you might want to subscribe outside of UI thread
                            }
                            subscriber.onCompleted();
                        } catch (InterruptedException e) {
                        }
                    }
                }).start();
            }
        });

    }
}
