package org.riksa.android.rxdaggerviewbinding.dagger;

import android.app.Application;

import dagger.Module;

/**
 * Created by riksa on 28/10/15.
 */
@Module
public class AndroidModule {
    private final Application application;

    public AndroidModule(Application application) {
        this.application = application;
    }

}
