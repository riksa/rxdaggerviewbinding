package org.riksa.android.rxdaggerviewbinding;

import android.app.Application;

import org.riksa.android.rxdaggerviewbinding.dagger.AndroidModule;
import org.riksa.android.rxdaggerviewbinding.dagger.ApplicationComponent;
import org.riksa.android.rxdaggerviewbinding.dagger.DaggerApplicationComponent;
import org.riksa.android.rxdaggerviewbinding.dagger.VariantModule;

import timber.log.Timber;

/**
 * Created by riksa on 28/10/15.
 */
public class MyApplication extends Application {
    private static ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.ENABLE_LOGGING) {
            Timber.plant(new Timber.DebugTree());
        }

        component = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .variantModule(new VariantModule())
                .build();

        component.inject(this);

    }

    public static ApplicationComponent getApplicationComponent() {
        return component;
    }
}
