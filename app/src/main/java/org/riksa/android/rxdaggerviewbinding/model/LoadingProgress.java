package org.riksa.android.rxdaggerviewbinding.model;

/**
 * Created by riksa on 28/10/15.
 */
public class LoadingProgress {
    public final int progress;
    public final String text;

    public LoadingProgress(int progress, String text) {
        this.progress = progress;
        this.text = text;
    }

    @Override
    public String toString() {
        return "LoadingProgress{" +
                "progress=" + progress +
                ", text='" + text + '\'' +
                '}';
    }
}
