package org.riksa.android.rxdaggerviewbinding.model;

/**
 * Created by riksa on 28/10/15.
 */
public class ViewModelCard {
    public String title;
    public String date;

    public ViewModelCard(String title, String date) {
        this.title = title;
        this.date = date;
    }

    @Override
    public String toString() {
        return "ViewModelCard{" +
                "title='" + title + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
