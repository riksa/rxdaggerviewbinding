package org.riksa.android.rxdaggerviewbinding;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.riksa.android.rxdaggerviewbinding.databinding.FragmentRepoListBinding;
import org.riksa.android.rxdaggerviewbinding.model.ViewModelCard;
import org.riksa.android.rxdaggerviewbinding.recyclerview.CardAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RepoListFragment extends Fragment {
    private static final String ARG_CARD_LIST = "arg_card_list";
    private CardAdapter adapter;
    private final List<ViewModelCard> cardList = new ArrayList<>();

    public RepoListFragment() {
        // Required empty public constructor
        MyApplication.getApplicationComponent().inject(this);
    }

    public static Fragment newInstance(ArrayList<ViewModelCard> cardList) {
        final RepoListFragment repoListFragment = new RepoListFragment();
        final Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_CARD_LIST, cardList);
        repoListFragment.setArguments(bundle);
        return repoListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            final List<ViewModelCard> cards = (List<ViewModelCard>) getArguments().getSerializable(ARG_CARD_LIST);
            if (cards != null) {
                cardList.addAll(cards);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentRepoListBinding binding = FragmentRepoListBinding.inflate(inflater, container, false);
        // nothing to bind yet

        final View root = binding.getRoot();

        final RecyclerView recyclerView = ButterKnife.findById(root, R.id.recycler_view);
        adapter = new CardAdapter(cardList);
        recyclerView.setAdapter(adapter);

        recyclerView.setHasFixedSize(true);

        final int spanCount = getResources().getInteger(R.integer.spancount_grid);
        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), spanCount, GridLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);

        return binding.getRoot();
    }

}
