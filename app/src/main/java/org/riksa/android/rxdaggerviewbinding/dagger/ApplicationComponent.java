package org.riksa.android.rxdaggerviewbinding.dagger;

import org.riksa.android.rxdaggerviewbinding.ConfigLoadingFragment;
import org.riksa.android.rxdaggerviewbinding.LoaderWorkerFragment;
import org.riksa.android.rxdaggerviewbinding.MyApplication;
import org.riksa.android.rxdaggerviewbinding.RepoListFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by riksa on 28/10/15.
 */
@Singleton
@Component(modules = {AndroidModule.class, CommonModule.class, org.riksa.android.rxdaggerviewbinding.dagger.VariantModule.class})
public interface ApplicationComponent {
    void inject(MyApplication application);

    void inject(LoaderWorkerFragment fragment);

    void inject(ConfigLoadingFragment fragment);

    void inject(RepoListFragment fragment);
}
