package org.riksa.android.rxdaggerviewbinding;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.riksa.android.rxdaggerviewbinding.databinding.FragmentConfigLoadingBinding;
import org.riksa.android.rxdaggerviewbinding.model.LoadingProgress;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigLoadingFragment extends Fragment {

    @Inject
    protected Bus bus;

    private FragmentConfigLoadingBinding binding;

    public ConfigLoadingFragment() {
        // Required empty public constructor
        MyApplication.getApplicationComponent().inject(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentConfigLoadingBinding.inflate(inflater, container, false);
        binding.setOnClickExample(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Subscribe
    public void onLoadingProgress(LoadingProgress loadingProgress) {
        Timber.d("Got progress %s", loadingProgress);
        binding.setLoadingProgress(loadingProgress);
    }
}
