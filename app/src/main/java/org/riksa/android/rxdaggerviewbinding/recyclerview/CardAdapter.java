package org.riksa.android.rxdaggerviewbinding.recyclerview;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.riksa.android.rxdaggerviewbinding.databinding.CardItemBinding;
import org.riksa.android.rxdaggerviewbinding.model.ViewModelCard;

import java.util.List;

/**
 * Created by riksa on 28/10/15.
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
    private final List<ViewModelCard> cardList;

    public CardAdapter(List<ViewModelCard> cardList) {
        this.cardList = cardList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final CardItemBinding binding = CardItemBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.binding.setCard(cardList.get(position));
        final String title = holder.binding.getCard().title;
        holder.binding.setOnCardClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(
                        v,
                        String.format("Clicked on #%d : %s", position, title),
                        Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final CardItemBinding binding;

        public ViewHolder(CardItemBinding cardItemBinding) {
            super(cardItemBinding.getRoot());
            this.binding = cardItemBinding;
        }
    }
}
