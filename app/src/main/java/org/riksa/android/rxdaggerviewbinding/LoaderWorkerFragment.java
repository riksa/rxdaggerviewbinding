package org.riksa.android.rxdaggerviewbinding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;

import org.riksa.android.rxdaggerviewbinding.model.LoadingProgress;
import org.riksa.android.rxdaggerviewbinding.model.ViewModelCard;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import icepick.State;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoaderWorkerFragment extends Fragment {
    private static final String TAG_CONTENT = "content";
    private boolean initialized = false; // this would in some (singleton) object in real life...

    @Inject
    protected Observable<LoadingProgress> loadingProgressObservable;
    protected Subscription loadingProgressSubscription;

    @Inject
    protected Observable<List<ViewModelCard>> cardListObservable;

    @Inject
    protected Bus bus;

    @State
    protected ArrayList<ViewModelCard> cardList;

    public LoaderWorkerFragment() {
        setRetainInstance(true);
        MyApplication.getApplicationComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!initialized) {
            subscribeToConfiguration();
        }
    }


    @Override
    public void onDestroy() {
        // do not rely on onDestroy getting called
        unsubscribeFromConfiguration();
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showContent();
    }

    private void unsubscribeFromConfiguration() {
        if (loadingProgressSubscription != null && !loadingProgressSubscription.isUnsubscribed()) {
            loadingProgressSubscription.unsubscribe();
            loadingProgressSubscription = null;
        }
    }

    private void subscribeToConfiguration() {
        if (loadingProgressSubscription == null) {
            loadingProgressObservable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<LoadingProgress>() {
                        @Override
                        public void onCompleted() {
                            initialized = true;
                            Timber.d("Done loading");
                            showContent();
                            // TODO: swap to content
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(e, e.getMessage());
                            // TODO
                        }

                        @Override
                        public void onNext(LoadingProgress loadingProgress) {
                            Timber.d("Got progress %s", loadingProgress);
                            bus.post(loadingProgress);
                        }
                    });

        }
    }

    private void showContent() {
        if (!initialized) {
            // not initialized, "show loading screen"
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new ConfigLoadingFragment(), TAG_CONTENT)
                    .commit();

        } else if (cardList == null) {
            // initialized, no cards, subscribe to them (could have been done as part of LoadingProgress
            cardListObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<ViewModelCard>>() {
                        @Override
                        public void onCompleted() {
                            showContent();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(e, e.getMessage()); // TODO: handle
                        }

                        @Override
                        public void onNext(List<ViewModelCard> viewModelCards) {
                            cardList = new ArrayList<>(viewModelCards);
                        }
                    });

        } else {
            // initialized and cardlist set
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, RepoListFragment.newInstance(cardList), TAG_CONTENT)
                    .commit();
        }
    }
}
