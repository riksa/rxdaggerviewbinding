package org.riksa.android.rxdaggerviewbinding.dagger;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by riksa on 28/10/15.
 */
@Module
public class CommonModule {

    @Singleton
    @Provides
    public Bus provideBus() {
        return new Bus();
    }
}
